import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './main.scss';

import {Excel} from './pages/excel'
import {GlobalStateProvider} from './AppComponent/state'

ReactDOM.render(
<GlobalStateProvider>
<Excel></Excel>
</GlobalStateProvider>, document.getElementById('app'));