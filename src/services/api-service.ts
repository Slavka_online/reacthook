import axios, {AxiosPromise} from 'axios';

export function searh(value: string): Promise<string[]> {
	return axios.get<string[]>(`https://beta.knoema.org/api/1.0/search/autocomplete?lang=en-US&limit=5&query=${value}&limit=4&client_id=EZj54KGFo3rzIvnLczrElvAitEyU28DGw9R73tif`)
	.then(r => r.data)
}