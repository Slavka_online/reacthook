import * as React from 'react'
import {Search} from '../components/search'
import {PreviewExcel} from '../components/preview-excel'
import {Login} from '../components/login'

import '../styles/excel-page.scss'

export const  Excel  = () => {
	return(
	<div className="excel-page">
		<div className="fixed">
			<Search></Search>
		</div>
		<div className="content">
			<PreviewExcel></PreviewExcel>
		</div>
		<div className="fixed">
			<Login></Login>
		</div>
	</div>
	)
}