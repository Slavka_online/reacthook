import * as React from 'react'
import '../styles/preview-excel.scss'

export const PreviewExcel = () => {
	return(
		<div className="previewExcel">
			<div className="text">
				<p>Knoema DataFinder automatically provides relevant data as you type in and edit your spreadsheet.</p>
				<p>Simply navigate to a cell, and any data that correlates to its text (for instance “lemons production Italy” or “USA unemployment”) will generate relevant data from Knoema that you can explore or insert from this task pane.</p>
			</div>
			<div className="image"></div>
		</div>
	)
}