import * as React from 'react'
import '../styles/search.scss'
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import * as Autosuggest from 'react-autosuggest';
import * as Api from '../services/api-service';
import * as search from  '../styles/search.scss';

const StringAutosuggest = Autosuggest as { new (): Autosuggest<string> }

type SearchState = {
	inputValue: string;
	suggestions: string[];
	mode: 'active' | 'preview'
}

const renderSuggestion = (suggestion: string) => (
	<div>
	  {suggestion}
	</div>
  );

export const Search = () => {

    let [state, setState]  = React.useState<SearchState>({inputValue: '', mode:'preview', suggestions: []});

	function setActiveMode() {
		setState(old => ({...old, mode: 'active'}))
	}

	function setPreviewMode() {
		setState(old => ({...old, mode: 'preview'}))
	}

	function setValue(value: string) {
		setState(old => ({...old, inputValue: value}))
	}

	function onChange (event: any,  {newValue}: any ) {
		setState(old => ({...old, inputValue: newValue}));
	}

	const searchProps = {
		placeholder: '',
		value: state.inputValue,
		onChange: onChange
	  };

	async function onSuggestionsFetchRequested({ value }: any) {
		var result = await Api.searh(value as string);
		setState(old => ({...old, inputValue: value, suggestions: result}));
	};
	
	function onSuggestionsClearRequested () {
		setState(old => ({...old, inputValue: '', suggestions: []}));
	};
	


	return (<div className="search">
			{state.mode === 'preview' 
			?
            <div className="preview" >
                <div className="icon" ></div>
                <div className="title">DataFinder</div>
                <div className="search-button" onClick={() => setActiveMode()}></div>
            </div>
			:
			<div className="active">
				 <div className="back" onClick={() => setPreviewMode()}></div>
				{/* <TextField className="text" label="Search:" underlined /> */}
				<StringAutosuggest
					theme={search}
					suggestions={state.suggestions}
					inputProps={searchProps}
					onSuggestionsFetchRequested={onSuggestionsFetchRequested}
					onSuggestionsClearRequested={onSuggestionsClearRequested}
					getSuggestionValue={(s) => s}
					renderSuggestion={renderSuggestion}

				></StringAutosuggest>

			</div>
			}
    </div>)
}