import * as React from 'react'
import '../styles/login.scss'

export const Login = () => {
	return(
		<div className="login">
			<div className="icon"></div>
			<div className="action">Log in</div>
		</div>
	)
}