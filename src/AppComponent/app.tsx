import * as React from 'react';
import {Counter} from './Counter'
import {useGlobalStore} from './state'
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';

export interface AppProps {
    name: string;
} 

export interface AppState {

}



export function AppComponent() {
    let {state, dispatch} = useGlobalStore();

    return(
        <>
        <Counter></Counter>
        <DefaultButton onClick = {() => dispatch({type: 'inc'})}>Update</DefaultButton>
        </>
    )
}