import * as React from 'react';
import {useGlobalStore} from './state'
export interface CounterProps {
    current: number;
}

interface CounterState {
    value: number;
}

export function Counter() {
    let {state, } = useGlobalStore();

    return (<div>{state.current}</div>)
}