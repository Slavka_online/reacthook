import * as React from 'react';


const  initState = {
    current: 10
}
type GlobalState = typeof initState;

type Inc = {
    type: 'inc'
}

type Actions = | Inc

let reducer: React.Reducer<GlobalState, Actions> = (state, action) => {
    switch(action.type) {
        case 'inc': {
            console.log("inc"); 
            return {...state, current: state.current + 1 }
        }
    }
}

const GlobalStateContext = React.createContext<{state: GlobalState, dispatch: React.Dispatch<Actions>}>({state: initState, dispatch: ((_) => initState)});

export const GlobalStateProvider = (props: any) =>  {
    let [state, dispatch] = React.useReducer(reducer, initState)
    let value = {state, dispatch };
    return (<GlobalStateContext.Provider value={value}>{props.children}</GlobalStateContext.Provider>);
}

export const useGlobalStore = () => {
    const { state, dispatch } = React.useContext(GlobalStateContext);
    return { state, dispatch };
  };
