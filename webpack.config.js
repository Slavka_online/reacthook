const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WebpackDevServer  = require("webpack-dev-server");


  module.exports = {
   entry: './src/index.tsx',
   devtool: 'inline-source-map',
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/
        },
        {
          test: /\.scss$/,
          use: [
              { loader:"style-loader"}, // creates style nodes from JS strings
              { loader:"css-loader"}, // translates CSS into CommonJS
              { loader:"sass-loader"}, // compiles Sass to CSS, using Node Sass by default
              { loader:"css-modules-typescript-loader" }
          ]
        },
        {
          test: /\.css$/i,
          use: [ { loader:"style-loader"}, {loader:"css-loader", options:{modules:true}}],
        },
        {
          test: /\.png$/,
          loader: "url-loader?limit=20000&minetype=image/jpg"
        },
      ]
    },
    resolve: {
      extensions: [ '.tsx', '.ts', '.js' ]
    },
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist')
    },
    externals: {
        "React": "react",
        "ReactDOM": "react-dom"
    },

    plugins: [
        new HtmlWebpackPlugin({
          template: "./public/index.html"
        }) 
      ],
      devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
      }

  };